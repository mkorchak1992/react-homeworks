import React, {Component} from 'react';
import './App.scss';
import Modal from './Components/Modal/Modal'
import Main from "./Components/Main/Main";
import Footer from "./Components/Footer/Footer";


class App extends Component {

    state = {
        Modal: false
    }

    showModal(event) {
        this.setState({Modal: true});
    }

    closeModal = () => {
        this.setState({Modal: false})
    }

    render() {
        return (
            <div className="App">
                <header className={"header"}>
                    <h1 className={"header-title"}>The best LapTops</h1>
                    <p className={'header-cart'}><i className="fas fa-shopping-cart"/></p>
                </header>
                {this.state.Modal ?
                    <Modal header={"success"} text={"\n" +
                    "The product has been added to the cart"} closeButton={true} actions={
                        <>
                            <button className={'modal__buttons-btn'}
                                    onClick={(event) => this.closeModal(event)}>
                                continue shopping
                            </button>
                            <button className={'modal__buttons-btn'}>
                               go to cart
                            </button>
                        </>
                    } onClick={() => this.closeModal()}/> : ''}
                <Main onClick={(event) => this.showModal(event)}/>
                <Footer/>
            </div>
        );
    }
}

export default App;