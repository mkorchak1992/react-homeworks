import React, {Component} from 'react';
import './ProductItem.scss';
import PropTypes from 'prop-types';

class ProductItem extends Component {

    constructor(props) {
        super(props);
        this.onClick = this.props.onClick;
    }

    state = {
        favorite: false
    }

    addToFavorite = (event) => {
        if (event.target.classList.contains('product-item-buyBtn')) {
            this.onClick(event);
            this.putToStorage('AllCartProducts');
        } else {
            localStorage.setItem(`Favorites Product ${this.props.article}`, JSON.stringify(this.props))
            this.setState({
                favorite: true
            })
        }
    }

    putToStorage(folder) {
        let existingProducts = JSON.parse(localStorage.getItem(folder));
        if (existingProducts == null) existingProducts = [];
        existingProducts.push(this.props);
        let ProductsUniqCart = Array.from(new Set(existingProducts.map(JSON.stringify))).map(JSON.parse);
        localStorage.setItem(folder, JSON.stringify(ProductsUniqCart));
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (localStorage.getItem(`Favorites Product ${this.props.article}`)) {
            this.putToStorage('Favorites');
        }
    }

    render() {

        const {state} = this.state;
        const {name, price, url, article, color} = this.props;
        return (
            <div className={'product-item__container'}>
                <img className={'product-item__photo'} src={url} alt={"laptop_photo"}/>
                <p className={'product-item__name'}>{name}</p>
                <p className={'product-item__price'}>Price - {price}$</p>
                <p className={'product-item__article'}>Article - {article}</p>
                <p className={'product-item__color'}>Color - {color}</p>
                {state || localStorage.getItem(`Favorites Product ${this.props.article}`)
                    ?
                    <span className={"product-item__choose-star-gold"}
                          onClick={(e) => this.addToFavorite(e)}>
        <i className="far fa-star"/></span>
                    :
                    <span className={"product-item__choose-star"}
                          onClick={(e) => this.addToFavorite(e)}>
        <i className="far fa-star"/></span>}
                <button className={"product-item-buyBtn"}
                        onClick={(event) => this.addToFavorite(event)}>Add to cart
                </button>
            </div>
        );
    }
}

export default ProductItem;


ProductItem.propTypes = {
    name: PropTypes.string,
    onClick: PropTypes.func,
    price: PropTypes.number,
    url: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
    favorite: PropTypes.bool
}

ProductItem.defaultProps = {
    price: 'not available',
    article: 'delivery is expected'
}