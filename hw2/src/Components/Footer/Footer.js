import React, {Component} from 'react';
import './Footer.scss'

class Footer extends Component {
  render() {
    return (
      <footer className={"footer"}>
        <p>&copy; Copyright 2020 | All Rights
          Reserved
        </p>
      </footer>
    );
  }
}

export default Footer;