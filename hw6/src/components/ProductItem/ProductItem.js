import React, {useState, useEffect} from 'react';
import './ProductItem.scss';
import PropTypes from 'prop-types';
import {AddToCartModal, closeModal} from "../store/modal/modalActions";
import {connect} from "react-redux";
import {addToFavAction, removeFromFavAction} from "../store/favorite/favoriteActions";

export const ProductItem = (props) => {

  const {name, price, url, article, color, AddToCartModal, elem, removeFromFavourites, addToFavourites} = props;
    const [favorite, setFavorite] = useState(false);

    useEffect(() => {
        if (localStorage.getItem(`Favorites Product ${article}`)) {
            setFavorite(true)
        }
    });

    return (
        <div className="product-item__container">
            <img className="product-item__photo" src={url} alt={"laptop_photo"}/>
            <p className="product-item__name">{name}</p>
            <p className="product-item__price">Price - {price}$</p>
            <p className="product-item__article">Article - {article}</p>
            <p className="product-item__color">Color - {color}</p>
            {favorite || localStorage.getItem(`Favorites Product ${article}`)
                ?
                <span data-testid={"remove-from-favoritesBtn"} className="product-item__choose-star-gold"
                      onClick={() => {
                          removeFromFavourites(elem);
                          setFavorite(false);
                      }}>
        <i className="far fa-star gold"/></span>
                :
                <span data-testid={"add-to-favoritesBtn"} className="product-item__choose-star"
                      onClick={() => {
                          addToFavourites(elem);
                          setFavorite(true);
                      }}>
        <i className="far fa-star"/></span>}
            <button data-testid={"product-item-buyBtn"} className="product-item-buyBtn"
                    onClick={() => AddToCartModal(elem)}>Add to cart
            </button>
        </div>
    );
};

const mapStoreToProps = (store) => ({
    myModal: store.modal
});

const mapDispatchToProps = (dispatch) => {
    return {
        AddToCartModal: (elem) => dispatch(AddToCartModal(elem)),
        closeModal: () => dispatch(closeModal()),
        removeFromFavourites: (elem) => dispatch(removeFromFavAction(elem)),
        addToFavourites: (elem) => dispatch(addToFavAction(elem))
    }
};

export default connect(mapStoreToProps, mapDispatchToProps)(ProductItem);

ProductItem.propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.string,
    url: PropTypes.string.isRequired,
    article: PropTypes.string.isRequired,
    color: PropTypes.string,
    favorite: PropTypes.bool,
    AddToCartModal: PropTypes.func,
    elem: PropTypes.object.isRequired,
    removeFromFavourites: PropTypes.func,
    addToFavourites: PropTypes.func
};

ProductItem.defaultProps = {
    price: 'not available',
    color: 'not available'
};
