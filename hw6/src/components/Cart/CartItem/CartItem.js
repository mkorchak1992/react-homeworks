import React from 'react';
import "../../ProductItem/ProductItem.scss";
import PropTypes from 'prop-types';
import {RemoveFromCartModal} from "../../store/modal/modalActions";
import {connect} from "react-redux";

export const CartItem = ({name, price, url, article, color, elem, RemoveFromCart}) => {

    return (
        <div className="product-item__container cart">
            <button data-testid={"remove-fromCartBtn"} className="product-item-remove-btn"
                    onClick={() => RemoveFromCart(elem)}>X
            </button>
            <img className="product-item__photo" src={url} alt={"laptop_photo"}/>
            <p className="product-item__name">{name}</p>
            <p className="product-item__price">Price - {price}$</p>
            <p className="product-item__article">Article - {article}</p>
            <p className="product-item__color">Color - {color}</p>
        </div>
    )
};

const mapStoreToProps = () => ({});
const mapDispatchToProps = (dispatch) => ({
    RemoveFromCart: (elem) => dispatch(RemoveFromCartModal(elem))
});
export default connect(mapStoreToProps, mapDispatchToProps)(CartItem);


CartItem.propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.string,
    url: PropTypes.string.isRequired,
    article: PropTypes.string.isRequired,
    color: PropTypes.string,
    RemoveFromCart: PropTypes.func,
    elem: PropTypes.object
};

CartItem.defaultProps = {
    price: 'not available',
    color: 'not available'
};
