import React from "react";
import {FavoritesItem} from "./FavoritesItem";
import {render} from "@testing-library/react";
import userEvent from "@testing-library/user-event";

const {click} = userEvent;

describe("Unit testing FavoritesItem in Favorites", () => {

    test("smoke test removeFavBtn", () => {
        const {getByTestId} = render(<FavoritesItem/>)
        const removeFavBtn = getByTestId("remove-from-favBtn");
        expect(removeFavBtn).toHaveClass("far fa-star")
    })

    test("when we click removeFavBtn - function is works", () => {
        const removeFromFav = jest.fn();
        const {getByTestId} = render(<FavoritesItem removeFromFavourites={removeFromFav}/>);
        const removeFavBtn = getByTestId("remove-from-favBtn");
        click(removeFavBtn);
        expect(removeFromFav).toBeCalled();
    })

})