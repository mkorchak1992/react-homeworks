import React from "react";
import {Modal} from "./Modal";
import {render} from "@testing-library/react";
import userEvent from "@testing-library/user-event";


const {click} = userEvent;

describe("Unit testing component Modal", () => {

    test("Modal fields have TextContent", () => {
        const {getByTestId} = render(<Modal header={'Header Field'} text={'text-field'}/>)
        const headerField = getByTestId("headerField");
        const textContField = getByTestId("textContField");

        expect(headerField.tagName).toBe('H2');
        expect(textContField.tagName).toBe('P');
        expect(headerField.textContent).toBe("Header Field")
        expect(textContField.textContent).toBe("text-field")
    })

    test("Modal Buttons start functions by Click", () => {
        const onClick = jest.fn();
        const {getByTestId} = render(<Modal header={'Header Field'} text={'text-field'} closeModal={onClick}/>)
        const modalBtns = getByTestId("modalBtns");
        const closeBtn = getByTestId("closeBtn");

        click(closeBtn);
        click(modalBtns);
        expect(onClick).toBeCalled;
    })

    test("Modal Button - crossCloseBtn is exist and work", () => {
        const onClick = jest.fn();
        const {getByTestId} = render(<Modal closeButton={true} header={'Header Field'} text={'text-field'}
                                            closeModal={onClick}/>)
        const crossCloseBtn = getByTestId("crossCloseBtn");

        click(crossCloseBtn);
        expect(getByTestId('crossCloseBtn')).not.toBe('null');
        expect(getByTestId('crossCloseBtn')).toBeDefined();
        expect(onClick).toBeCalled;
    })
})

