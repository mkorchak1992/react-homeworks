import React, {useEffect} from 'react';
import "./Main.scss"
import ProductItem from "../ProductItem/ProductItem";
import {itemsLoadedAction} from "../store/items/itemsActions";
import {connect} from "react-redux";
import PropTypes from "prop-types";

export const Main = ({loadItems, items, myModal}) => {

    useEffect(() => {
        loadItems();
    }, [loadItems]);

    const products = items.map((el, index) =>
        <ProductItem
            name={el.name}
            price={parseFloat(el.price)}
            url={el.url}
            article={el.article}
            color={el.color}
            key={index}
            elem={el}
        />);

    return (
        <>
            {myModal || ''}
            <div data-testid={"main-page"} className="main">
                {products}
            </div>
        </>

    );
};

const mapStoreToProps = (store) => ({
    items: store.items.result,
    myModal: store.modal

});

const mapDispatchToProps = (dispatch) => {
    return {
        loadItems: (items) => dispatch(itemsLoadedAction(items)),
    }
};

export default connect(mapStoreToProps, mapDispatchToProps)(Main);

Main.propTypes = {
    loadItems: PropTypes.func.isRequired,
    items: PropTypes.array.isRequired,
    myModal: PropTypes.object
};
