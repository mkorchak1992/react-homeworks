export const LOAD_FAVORITE_START = "LOAD_FAVORITE_START";
export const REMOVE_FROM_FAVOR = "REMOVE_FROM_FAVOR";
export const ADD_TO_FAVOR = "ADD_TO_FAVOR";

export const favoriteLoadedAction = () => dispatch => {
    const allFavItems = JSON.parse(localStorage.getItem('Favorites'));
    dispatch({
        type: LOAD_FAVORITE_START,
        allFavItems
    })
};

export const removeFromFavAction = (elem) => dispatch => {
    let allFavItems = JSON.parse(localStorage.getItem('Favorites'));
    localStorage.removeItem(`Favorites Product ${elem.article}`);
    allFavItems = allFavItems.filter((item => item.article !== elem.article));
    localStorage.setItem('Favorites', JSON.stringify(allFavItems));
    dispatch({
        type: REMOVE_FROM_FAVOR,
        allFavItems
    })
};

export const addToFavAction = (elem) => dispatch => {
    localStorage.setItem(`Favorites Product ${elem.article}`, JSON.stringify(elem));
    let existingProducts = JSON.parse(localStorage.getItem('Favorites'));
    if (existingProducts == null) existingProducts = [];
    existingProducts.push(elem);
    let ProductsUniq = Array.from(new Set(existingProducts.map(JSON.stringify))).map(JSON.parse);
    localStorage.setItem('Favorites', JSON.stringify(ProductsUniq));
};