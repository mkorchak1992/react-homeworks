import axios from "axios";

export const ITEMS_LOADING_START = "ITEMS_LOADING_START";
export const ITEMS_LOADING_END = "ITEMS_LOADING_FINISH";
export const ITEMS_LOADING_ERROR = "ITEMS_LOADING_ERROR";

export function itemsLoadedAction() {
    return (dispatch) => {
        dispatch({type: ITEMS_LOADING_START});
        axios("./productsStore.json")
            .then(res => {
                dispatch({
                    type: ITEMS_LOADING_END,
                    result: res.data
                })
            })
            .catch(error => {
                dispatch({type: ITEMS_LOADING_ERROR, error})
            })
    }
}