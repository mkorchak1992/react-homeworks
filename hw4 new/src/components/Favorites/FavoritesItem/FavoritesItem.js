import React from "react";
import "../../ProductItem/ProductItem.scss";
import PropTypes from "prop-types";
import CartItem from "../../Cart/CartItem/CartItem";
import {removeFromFavAction} from "../../store/favorite/favoriteActions";
import {connect} from "react-redux";

const FavoritesItem = (props) => {

    const {name, price, url, article, color, elem, removeFromFavourites} = props;

    return (
        <div className="product-item__container">
            <img className="product-item__photo" src={url} alt={"laptop_photo"}/>
            <p className="product-item__name">{name}</p>
            <p className="product-item__price">Price - {price}$</p>
            <p className="product-item__article">Article - {article}</p>
            <p className="product-item__color">Color - {color}</p>
            <p className="product-item__choose-star-gold active">
                <i className="far fa-star" onClick={() => removeFromFavourites(elem)}/></p>
        </div>
    );
};
const mapStoreToProps = (store) => ({});
const mapDispatchToProps = (dispatch) => ({
    removeFromFavourites: (elem) => dispatch(removeFromFavAction(elem))
});

export default connect(mapStoreToProps, mapDispatchToProps)(FavoritesItem);


CartItem.propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.number,
    url: PropTypes.string.isRequired,
    article: PropTypes.string.isRequired,
    color: PropTypes.string,
    removeFromFavourites:PropTypes.func,
    elem:PropTypes.object
};

CartItem.defaultProps = {
    price: 'not available',
    color: 'not available'
};
