import React, {useEffect} from 'react';
import FavoritesItem from "./FavoritesItem/FavoritesItem";
import "./Favorites.scss"
import {connect} from "react-redux";
import {favoriteLoadedAction} from "../store/favorite/favoriteActions";
import PropTypes from "prop-types";

const Favorites = ({loadFavoritesProducts, allFavoritesProducts}) => {

    useEffect(() => {
        loadFavoritesProducts();
    }, [loadFavoritesProducts]);


    if (allFavoritesProducts === null || allFavoritesProducts[0] === undefined) {
        return (
            <h2 className="favorites__section-text">There is no favourites laptops here...</h2>
        )
    } else {
        let favoriteItem = allFavoritesProducts.map((el, index) =>
            <FavoritesItem
                name={el.name}
                price={parseFloat(el.price)}
                url={el.url}
                article={el.article}
                color={el.color}
                key={index}
                elem ={el}
            />);

        return (
            <div className="main">
                {favoriteItem}
            </div>
        )
    }

};

const mapStoreToProps = (store) => ({
    allFavoritesProducts: store.AllFavoritesProducts
});

const mapDispatchToProps = (dispatch) => ({
    loadFavoritesProducts: () => dispatch(favoriteLoadedAction())
});

export default connect(mapStoreToProps, mapDispatchToProps)(Favorites);


Favorites.propTypes = {
    AllFavoritesProducts: PropTypes.array,
    loadFavoritesProducts: PropTypes.func
};