import {combineReducers} from "redux";
import itemsReducer from "./items/itemsReducer";
import modalReducer from "./modal/modalReducer";
import cartItemsReducer from "./cart/cartReducer";
import favoriteItemsReducer from "./favorite/favoriteReducer";


export default combineReducers({
    items: itemsReducer,
    modal: modalReducer,
    AllCartProducts: cartItemsReducer,
    AllFavoritesProducts: favoriteItemsReducer
})