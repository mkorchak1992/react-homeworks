export const LOAD_CART = "LOAD_CART_START ";
export const ADD_TO_CART="ADD_TO_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";

export const cartLoadedAction = () => dispatch => {
    const AllCartProducts = JSON.parse(localStorage.getItem('AllCartProducts'));
    dispatch({type:LOAD_CART,AllCartProducts})
};

export const addToCart = elem =>dispatch=>{
    let existingProducts = JSON.parse(localStorage.getItem('AllCartProducts'));
    if (existingProducts == null) existingProducts = [];
    existingProducts.push(elem);
    let ProductsUniqCart = Array.from(new Set(existingProducts.map(JSON.stringify))).map(JSON.parse);
    let AllCartProducts = localStorage.setItem('AllCartProducts', JSON.stringify(ProductsUniqCart));

    dispatch({type:ADD_TO_CART,AllCartProducts})
};

export const removeFromCart = elem =>dispatch=>{
    let AllCartProducts = JSON.parse(localStorage.getItem('AllCartProducts'));
    AllCartProducts = AllCartProducts.filter(item=>item.article !==elem.article);
    localStorage.setItem('AllCartProducts',JSON.stringify(AllCartProducts));

    dispatch({type:REMOVE_FROM_CART,AllCartProducts})
};