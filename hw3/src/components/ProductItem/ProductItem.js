import React, {useState, useEffect} from 'react';
import './ProductItem.scss';
import PropTypes from 'prop-types';


const ProductItem = (props) => {

    const {onClick} = props;
    const [favorite, setFavorite] = useState(false);

    const removeItem = ()=>{
        localStorage.removeItem(`Favorites Product ${article}`);
        setFavorite(false);
        let arr = JSON.parse(localStorage.getItem('Favorites')).filter((item => item.article !== article));
        localStorage.setItem('Favorites', JSON.stringify(arr));
    }

    const addToFavorite = (event) => {
            localStorage.setItem(`Favorites Product ${article}`, JSON.stringify(props));
            setFavorite(true);
    };

    const addToCart = (event) => {
        onClick(event);
        putToStorage('AllCartProducts');
        let newArr = JSON.parse(localStorage.getItem('AllCartProducts')).filter((item => item));
        localStorage.setItem('AllCartProducts', JSON.stringify(newArr));
    }

    const putToStorage = (folder) => {
        let existingProducts = JSON.parse(localStorage.getItem(folder));
        if (existingProducts == null) existingProducts = [];
        existingProducts.push(props);
        let ProductsUniqCart = Array.from(new Set(existingProducts.map(JSON.stringify))).map(JSON.parse);
        localStorage.setItem(folder, JSON.stringify(ProductsUniqCart));
    };

    useEffect(() => {
        if (localStorage.getItem(`Favorites Product ${article}`)) {
            putToStorage('Favorites');
            setFavorite(true)
        }

    });

    const {name, price, url, article, color} = props;
    return (
        <div className="product-item__container">
            <img className="product-item__photo" src={url} alt={"laptop_photo"}/>
            <p className="product-item__name">{name}</p>
            <p className="product-item__price">Price - {price}$</p>
            <p className="product-item__article">Article - {article}</p>
            <p className="product-item__color">Color - {color}</p>
            {favorite || localStorage.getItem(`Favorites Product ${article}`)
                ?
                <span className="product-item__choose-star-gold"
                      onClick={(e) => removeItem(e)}>
        <i className="far fa-star gold"/></span>
                :
                <span className="product-item__choose-star"
                      onClick={(e) => addToFavorite(e)}>
        <i className="far fa-star"/></span>}
            <button className="product-item-buyBtn"
                    onClick={(event) => addToCart(event)}>Add to cart
            </button>
        </div>
    );
};

export default ProductItem;


ProductItem.propTypes = {
    name: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    price: PropTypes.number,
    url: PropTypes.string.isRequired,
    article: PropTypes.string.isRequired,
    color: PropTypes.string,
    favorite: PropTypes.bool
};

ProductItem.defaultProps = {
    price: 'not available',
    color: 'not available'
};
