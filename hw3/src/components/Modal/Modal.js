import React from 'react';
import './Modal.scss';
import PropTypes from 'prop-types';

const Modal = (props) =>  {

        const {header, text, actions, closeButton,onClick} = props;

        return (
            <>
                <div className="modal__container">
                    {closeButton ? <button className="modal__container-closeBtn" onClick = {onClick}>X</button> :''}
                    <h2 className="modal__container-title">{header}</h2>
                    <p className="modal__container-text">{text}</p>
                    <div className="modal__buttons">{actions}</div>
                </div>
                <div className="modal-wrapper" onClick = {onClick}/>
            </>
        );
    };

export default Modal;

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  actions: PropTypes.object.isRequired,
  onClick: PropTypes.func,
  closeButton:PropTypes.bool
};
