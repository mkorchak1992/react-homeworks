import React from 'react';
import {useState} from 'react';
import './App.scss';
import Modal from './components/Modal/Modal';
import Footer from "./components/Footer/Footer";
import {NavLink} from "react-router-dom";
import AppRoutes from "./components/AppRoutes/routes";


const App = () => {
    const [modal, setModal] = useState(false);

    const showModal = (event) => {
        setModal(true);
    };

    const closeModal = () => {
        setModal(false);
    };

    return (
        <div className="App">
            <header className="header">
                <h1 className="header-title">The best LapTops</h1>
                <NavLink className="menu-link" exact activeClassName="active" to={'/'}>Main Page</NavLink>
                <NavLink className="menu-link" exact activeClassName="active" to={'/Cart'}>Cart</NavLink>
                <NavLink className="menu-link" exact activeClassName="active" to={'/Favorites'}>Favorites</NavLink>
                <NavLink to={'/Cart'}><p className="header-cart"><i className="fas fa-shopping-cart"/></p></NavLink>
            </header>

            {modal
                ? <Modal header={"success"} text={"\n" +
                "The product has been added to the cart"} closeButton={true} actions={
                        <button
                            className="modal__buttons-btn"
                            onClick={(event) => closeModal(event)}>
                            continue shopping
                        </button>
                } onClick={closeModal}/>
                : ''}

            <AppRoutes onClick={showModal}/>

            <Footer/>

        </div>
    );
};

export default App;