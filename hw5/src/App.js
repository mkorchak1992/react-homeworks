import React from 'react';
import './App.scss';
import Footer from "./components/Footer/Footer";
import {NavLink} from "react-router-dom";
import AppRoutes from "./components/AppRoutes/routes";

const App = () => {

    return (
        <div className="App">

            <header className="header">
                <h1 className="header-title">\*_*/ LapTops</h1>
                <NavLink className="menu-link" exact activeClassName="active" to={'/'}>Main Page</NavLink>
                <NavLink className="menu-link" exact activeClassName="active" to={'/Cart'}>Cart</NavLink>
                <NavLink className="menu-link" exact activeClassName="active" to={'/Favorites'}>Favorites</NavLink>
                <NavLink to={'/Cart'}><p className="header-cart"><i className="fas fa-shopping-cart"/></p></NavLink>
            </header>

            <AppRoutes/>

            <Footer/>

        </div>
    );
};


export default App;