import React, {useEffect} from 'react';
import './Cart.css';
import CartItem from "./CartItem/CartItem";
import {connect} from "react-redux";
import {cartLoadedAction} from "../store/cart/cartActions";
import PropTypes from "prop-types";
import BuyingForm from "../CheckoutForm/CheckoutForm";

const Cart = ({AllCartProducts, loadCartProducts, myModal}) => {

    useEffect(() => {
        loadCartProducts();
    }, [loadCartProducts]);

    if (!AllCartProducts || !AllCartProducts[0]) {
        return (
            <h2 className="cart__section-text">There is no products in the Cart...</h2>
        )
    } else {
        const cartItem = AllCartProducts.map((el, index) =>
            <CartItem
                name={el.name}
                price={parseFloat(el.price)}
                url={el.url}
                article={el.article}
                color={el.color}
                key={index}
                elem={el}
            />);

        return (
            <>
                <BuyingForm/>
                {myModal || ''}
                <div className="cart__products">
                    {cartItem}
                </div>

            </>

        )
    }
};


const mapStoreToProps = (store) => ({
    AllCartProducts: store.AllCartProducts,
    myModal: store.modal
});

const mapDispatchToProps = (dispatch) => ({
    loadCartProducts: () => dispatch(cartLoadedAction())
});

export default connect(mapStoreToProps, mapDispatchToProps)(Cart);

Cart.propTypes = {
    AllCartProducts: PropTypes.array,
    loadCartProducts: PropTypes.func.isRequired,
};
