import React from "react";
import {Route, Switch} from "react-router-dom";
import Main from "../Main/Main";
import Favorites from "../Favorites/Favorites";
import Cart from "../Cart/Cart";

const AppRoutes = () => {
    return (
        <Switch>
            <Route exact path="/" component={Main}/>
            <Route exact path="/Favorites" component={Favorites}/>
            <Route exact path="/Cart" component={Cart}/>
        </Switch>
    );
};

export default AppRoutes;

