import {applyMiddleware, compose, createStore} from "redux";
import initialStore from "./initialStore";
import thunk from "redux-thunk";
import rootReducer from "./rootReducer";


const store = createStore(
    rootReducer,
    initialStore,
    compose(
        applyMiddleware(thunk),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    )
);

export default store;