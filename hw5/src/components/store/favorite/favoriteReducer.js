import {LOAD_FAVORITE_START, ADD_TO_FAVOR, REMOVE_FROM_FAVOR} from "./favoriteActions";
import initialStore from "../initialStore";

export default function favoriteItemsReducer(MyFavoritesStore = initialStore.AllFavoritesProducts, {allFavItems, type}) {
    switch (type) {
        case LOAD_FAVORITE_START:
            return allFavItems;
        case REMOVE_FROM_FAVOR:
            return allFavItems;
        case ADD_TO_FAVOR:
            return allFavItems;
        default:
            return MyFavoritesStore;
    }
}